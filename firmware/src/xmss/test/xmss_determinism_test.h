#ifndef _XMSS_DETERMINISM_TEST_H_
#define _XMSS_DETERMINISM_TEST_H_

/**
 * Performs XMSS Determinism Test
 */
int xmss_determinism_test();

#endif