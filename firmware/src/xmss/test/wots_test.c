#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "../wots.h"
#include "../randombytes.h"
#include "../params.h"

int wots_test()
{
    xmss_params params;
    // TODO test more different OIDs
    uint32_t    oid = 0x01000001;
    int         i   = 0;

    /* For WOTS it doesn't matter if we use XMSS or XMSSMT. */
    xmss_parse_oid(&params, oid);

    unsigned char seed[params.n];
    unsigned char pub_seed[params.n];
    unsigned char pk1[params.wots_sig_bytes];
    unsigned char pk2[params.wots_sig_bytes];
    unsigned char sig[params.wots_sig_bytes];
    unsigned char m[params.n];
    uint32_t      addr[8] = { 0 };

    randombytes(seed,     params.n);
    randombytes(pub_seed, params.n);
    randombytes(m,        params.n);
    randombytes((unsigned char *)addr, 8 * sizeof(uint32_t));

    printf("Testing WOTS signature and PK derivation...");
    
//    printf("\n\nContents of a structure params are:\n");
//    printf("func:           %d\n",            params.func);
//    printf("n:              %d\n",            params.n);
//    printf("wots_w:         %d\n",            params.wots_w);
//    printf("wots_log_w:     %d\n",            params.wots_log_w);
//    printf("wots_len1:      %d\n",            params.wots_len1);
//    printf("wots_len2:      %d\n",            params.wots_len2);
//    printf("wots_len:       %d\n",            params.wots_len);
//    printf("wots_sig_bytes: %d\n",            params.wots_sig_bytes);
//    printf("full_height:    %d\n",            params.full_height);
//    printf("tree_height:    %d\n",            params.tree_height);
//    printf("d:              %d\n",            params.d);
//    printf("index_bytes:    %d\n",            params.index_bytes);
//    printf("sig_bytes:      %d\n",            params.sig_bytes);
//    printf("pk_bytes:       %d\n",            params.pk_bytes);
//    printf("sk_bytes:       0x%llx\n",        params.sk_bytes);
//    printf("bds_k:          %d\n",            params.bds_k);

    wots_pkgen(      &params, pk1,          seed, pub_seed, addr);
    wots_sign(       &params,      sig,  m, seed, pub_seed, addr);
    wots_pk_from_sig(&params, pk2, sig,  m,       pub_seed, addr);
    
//    printf("\n\nFirst 32 bytes of PK1: ");
//    for (i = 0; i < 32; ++i)
//    {
//        printf("[%d]=0x%02x ", i, *(pk1 + i) & 0xff);
//    }
//    printf("\nFirst 32 bytes of PK2: ");
//    for (i = 0; i < 32; ++i)
//    {
//        printf("[%d]=0x%02x ", i, *(pk2 + i) & 0xff);
//    }
//    printf("\n");

    if (memcmp(pk1, pk2, params.wots_sig_bytes))
    {
        printf("failed!\n");

        return -1;
    }
    printf("successful.\n");

    return 0;
}
