/*
 * This code was taken from the SPHINCS reference implementation and is public domain.
 */

#include <fcntl.h>
#include "xmss_rand_openssl_aes.h"

/* Linux Approach */
/*
#include <unistd.h>

static int fd = -1;

void randombytes(unsigned char *x, unsigned long long xlen)
{
    int i;

    if (fd == -1)
    {
        for (;;)
        {
            fd = open("/dev/urandom", O_RDONLY);
            if (fd != -1)
            {
                break;
            }
            sleep(1);
        }
    }

    while (xlen > 0)
    {
        if (xlen < 1048576)
        {
            i = xlen;
        }
        else
        {
            i = 1048576;
        }

        i = read(fd, x, i);
        if (i < 1)
        {
            sleep(1);
            continue;
        }

        x    += i;
        xlen -= i;
    }
}

*/

/* JMR Modified approach using AES Encryption */
void randombytes(unsigned char *x, unsigned long xlen)
{
    uint64_t rand64 = 0;
    unsigned char bytes[8];
    int i;
    int j;

    while (xlen > 0)
    {
        rand64 = RANDOM64;
        
        ull_to_bytes(bytes, 8, rand64);
        
//        printf("randombytes called with len=%u\n", xlen);
//        printf("rand64=0x%" PRIx64 "\n", rand64);
//        printf("x base address =0x%" PRIXPTR "\n", (uintptr_t)x);
        
        if (xlen < 8)
        {
            i = xlen;
        }
        else
        {
            i = 8;
        }
        
        for (j = 0; j < i; ++j)
        {
            *(x + j) = bytes[j];
        }
        
//        printf("bytes:       ");
//        for (j = 0; j < 8; ++j)
//        {
//            printf("[%d]=0x%02x ", j, bytes[j]);
//        }
//        printf("\n");
//        printf("x:           ");
//        for (j = 0; j < 8; ++j)
//        {
//            printf("[%d]=0x%02x ", j, bytes[j]);
//        }
//        printf("\n");

        x    += i;
        xlen -= i;
    }
}
