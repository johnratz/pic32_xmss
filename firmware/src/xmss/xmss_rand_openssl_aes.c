/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    xmss_rand_openssl_aes.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

#include "xmss_rand_openssl_aes.h"


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */

CRYPT_RNG_CTX        mcRng; // context that holds info relative to random number generation

/* Initialize AES */
static unsigned char aes_key[AES_BLOCK_SIZE];
static unsigned char aes_ivec[AES_BLOCK_SIZE];
static unsigned char aes_ecount_buf[AES_BLOCK_SIZE];
static unsigned int  aes_num = 0;
static unsigned char aes_in[AES_BLOCK_SIZE];
static CRYPT_AES_CTX enc;


/* ************************************************************************** */
/* ************************************************************************** */
// Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */

/*  A brief description of a section can be given directly below the section
    banner.
 */

void init_rng()
{
    CRYPT_RNG_Initialize(&mcRng);                             // initializes the CRYPT_RNG_CTX context at &mcRng
    CRYPT_RNG_BlockGenerate(&mcRng, aes_key, AES_BLOCK_SIZE); // warning: may return all zeros in simulator
    CRYPT_AES_KeySet(&enc, aes_key, AES_BLOCK_SIZE, NULL, AES_ENCRYPTION);
    memset(aes_ivec, 0, AES_BLOCK_SIZE);
    aes_ivec[0] = 0x01;
    memset(aes_ecount_buf, 0, AES_BLOCK_SIZE);
    memset(aes_in, 0, AES_BLOCK_SIZE);
}

uint64_t randomplease(int numbytes)
{
#ifdef DETERMINISTIC
    /* for debugging only */
    static uint64_t x = 0xdeadbeefcafebabeULL;     // a static variable inside a function keeps its value between invocations
    const uint64_t  a = 2862933555777941757ULL;
    const uint64_t  b = 3037000493ULL;
    x = a * x + b;

    return x;
#else
    uint64_t      x = 0;
    unsigned char buf[8];
    int           i;
    if (numbytes >= 8)
    {
        numbytes = 8;
    }
    // assuming little-endian
    my_AES_ctr128_encrypt(aes_in, (unsigned char *)&buf, numbytes, &enc, aes_ivec, aes_ecount_buf, &aes_num);
    for (i = 0; i < numbytes; i++)
    {
        x <<= 8;
        x  |= (uint64_t)buf[i];
    }

    return x;
#endif
}


/* *****************************************************************************
 End of File
 */
