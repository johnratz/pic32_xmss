/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

APP_DATA appData;

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback functions.
*/

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************


/* TODO:  Add any necessary local functions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    appData.state = APP_STATE_INIT;

    /* Initialize Random Number Generator */
    init_rng();
}


/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Tasks ( void )
{
    int        status     = false;
    static int firstPrint = 1;
    
    /* Check the application's current state. */
    switch ( appData.state )
    {
        /* Application's initial state. */
        case APP_STATE_INIT:
        {
            bool appInitialized = true;
       
        
            if (appInitialized)
            {
//                printf("Transitioning to APP_STATE_WOTS_TEST\n");
//                appData.state = APP_STATE_WOTS_TEST;
//                printf("Transitioning straight to APP_STATE_XMSS_DETERMINISM_TEST\n");
//                appData.state = APP_STATE_XMSS_DETERMINISM_TEST;
                printf("Transitioning straight to APP_STATE_XMSS_TEST\n");
                appData.state = APP_STATE_XMSS_TEST;
            }
            break;
        }
        
        
        case APP_STATE_WOTS_TEST:
        {
            status        = wots_test();
            if (status < 0)
            {
                printf("Transitioning to APP_STATE_ERROR\n");
                appData.state = APP_STATE_ERROR;
            }
            else
            {
                printf("Transitioning to APP_STATE_XMSS_DETERMINISM_TEST\n");
                appData.state = APP_STATE_XMSS_DETERMINISM_TEST;
            }
            break;
        }
        
        case APP_STATE_XMSS_DETERMINISM_TEST:
        {
            status        = xmss_determinism_test();
            if (status < 0)
            {
                printf("Transitioning to APP_STATE_ERROR\n");
                appData.state = APP_STATE_ERROR;
            }
            else
            {
                printf("Transitioning to APP_STATE_SPIN\n");
                appData.state = APP_STATE_SPIN;
            }
            break;
        }
        
        case APP_STATE_XMSS_TEST:
        {
            status        = xmss_test();
            if (status < 0)
            {
                printf("Transitioning to APP_STATE_ERROR\n");
                appData.state = APP_STATE_ERROR;
            }
            else
            {
                printf("Transitioning to APP_STATE_SPIN\n");
                appData.state = APP_STATE_SPIN;
            }
            break;
        }

        case APP_STATE_ERROR:
        {
            BSP_LED_2Off();
            BSP_LED_1On();
            if (firstPrint)
            {
                printf("Reached APP_STATE_ERROR\n");
                firstPrint = 0;
            }
            break;
        }

        case APP_STATE_SPIN:
        {
            BSP_LED_2Off();
            BSP_LED_3On();
            if (firstPrint)
            {
                printf("Reached APP_STATE_SPIN\n");
                firstPrint = 0;
            }
            break;
        }

        /* TODO: implement your application state machine.*/
        

        /* The default state should never be executed. */
        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }
    }
}

 

/*******************************************************************************
 End of File
 */
