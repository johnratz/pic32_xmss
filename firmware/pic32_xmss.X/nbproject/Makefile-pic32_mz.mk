#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-pic32_mz.mk)" "nbproject/Makefile-local-pic32_mz.mk"
include nbproject/Makefile-local-pic32_mz.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=pic32_mz
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/pic32_xmss.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/pic32_xmss.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../../../../framework/crypto/src/misc.c ../src/system_config/pic32_mz/bsp/bsp.c ../src/system_config/pic32_mz/framework/system/clk/src/sys_clk_pic32mz.c ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon.c ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon_pic32mz.c ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon_cache_pic32mz.S ../src/system_config/pic32_mz/framework/system/ports/src/sys_ports_static.c ../src/system_config/pic32_mz/system_init.c ../src/system_config/pic32_mz/system_interrupt.c ../src/system_config/pic32_mz/system_exceptions.c ../src/system_config/pic32_mz/system_tasks.c ../src/app.c ../src/main.c ../../../../framework/crypto/src/zlib-1.2.7/adler32.c ../../../../framework/crypto/src/zlib-1.2.7/compress.c ../../../../framework/crypto/src/zlib-1.2.7/crc32.c ../../../../framework/crypto/src/zlib-1.2.7/deflate.c ../../../../framework/crypto/src/zlib-1.2.7/infback.c ../../../../framework/crypto/src/zlib-1.2.7/inffast.c ../../../../framework/crypto/src/zlib-1.2.7/inflate.c ../../../../framework/crypto/src/zlib-1.2.7/inftrees.c ../../../../framework/crypto/src/zlib-1.2.7/trees.c ../../../../framework/crypto/src/zlib-1.2.7/uncompr.c ../../../../framework/crypto/src/zlib-1.2.7/zutil.c ../../../../framework/crypto/src/ecc.c ../../../../framework/crypto/src/arc4.c ../../../../framework/crypto/src/pwdbased.c ../../../../framework/crypto/src/tfm.c ../../../../framework/crypto/src/asn.c ../../../../framework/crypto/src/des3.c ../../../../framework/crypto/src/rsa.c ../../../../framework/crypto/src/aes.c ../../../../framework/crypto/src/md5.c ../../../../framework/crypto/src/sha.c ../../../../framework/crypto/src/sha256.c ../../../../framework/crypto/src/sha512.c ../../../../framework/crypto/src/hmac.c ../../../../framework/crypto/src/hash.c ../../../../framework/crypto/src/compress.c ../../../../framework/crypto/src/random.c ../../../../framework/crypto/src/crypto.c ../../../../framework/crypto/src/coding.c ../../../../framework/crypto/src/error.c ../../../../framework/crypto/src/integer.c ../../../../framework/crypto/src/logging.c ../../../../framework/crypto/src/memory.c ../../../../framework/crypto/src/wc_port.c ../../../../framework/crypto/src/wolfmath.c ../../../../framework/system/int/src/sys_int_pic32.c ../src/xmss/fips202.c ../src/xmss/hash.c ../src/xmss/hash_address.c ../src/xmss/params.c ../src/xmss/randombytes.c ../src/xmss/utils.c ../src/xmss/wots.c ../src/xmss/xmss.c ../src/xmss/xmss_commons.c ../src/xmss/xmss_core.c ../src/xmss/xmss_core_fast.c ../src/xmss/xmss_rand_openssl_aes.c ../src/xmss/test/wots_test.c ../src/xmss/test/xmss_determinism_test.c ../src/xmss/test/xmss_test.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1886890887/misc.o ${OBJECTDIR}/_ext/1108542646/bsp.o ${OBJECTDIR}/_ext/1632208797/sys_clk_pic32mz.o ${OBJECTDIR}/_ext/2139738052/sys_devcon.o ${OBJECTDIR}/_ext/2139738052/sys_devcon_pic32mz.o ${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o ${OBJECTDIR}/_ext/602991347/sys_ports_static.o ${OBJECTDIR}/_ext/625345382/system_init.o ${OBJECTDIR}/_ext/625345382/system_interrupt.o ${OBJECTDIR}/_ext/625345382/system_exceptions.o ${OBJECTDIR}/_ext/625345382/system_tasks.o ${OBJECTDIR}/_ext/1360937237/app.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1606841020/adler32.o ${OBJECTDIR}/_ext/1606841020/compress.o ${OBJECTDIR}/_ext/1606841020/crc32.o ${OBJECTDIR}/_ext/1606841020/deflate.o ${OBJECTDIR}/_ext/1606841020/infback.o ${OBJECTDIR}/_ext/1606841020/inffast.o ${OBJECTDIR}/_ext/1606841020/inflate.o ${OBJECTDIR}/_ext/1606841020/inftrees.o ${OBJECTDIR}/_ext/1606841020/trees.o ${OBJECTDIR}/_ext/1606841020/uncompr.o ${OBJECTDIR}/_ext/1606841020/zutil.o ${OBJECTDIR}/_ext/1886890887/ecc.o ${OBJECTDIR}/_ext/1886890887/arc4.o ${OBJECTDIR}/_ext/1886890887/pwdbased.o ${OBJECTDIR}/_ext/1886890887/tfm.o ${OBJECTDIR}/_ext/1886890887/asn.o ${OBJECTDIR}/_ext/1886890887/des3.o ${OBJECTDIR}/_ext/1886890887/rsa.o ${OBJECTDIR}/_ext/1886890887/aes.o ${OBJECTDIR}/_ext/1886890887/md5.o ${OBJECTDIR}/_ext/1886890887/sha.o ${OBJECTDIR}/_ext/1886890887/sha256.o ${OBJECTDIR}/_ext/1886890887/sha512.o ${OBJECTDIR}/_ext/1886890887/hmac.o ${OBJECTDIR}/_ext/1886890887/hash.o ${OBJECTDIR}/_ext/1886890887/compress.o ${OBJECTDIR}/_ext/1886890887/random.o ${OBJECTDIR}/_ext/1886890887/crypto.o ${OBJECTDIR}/_ext/1886890887/coding.o ${OBJECTDIR}/_ext/1886890887/error.o ${OBJECTDIR}/_ext/1886890887/integer.o ${OBJECTDIR}/_ext/1886890887/logging.o ${OBJECTDIR}/_ext/1886890887/memory.o ${OBJECTDIR}/_ext/1886890887/wc_port.o ${OBJECTDIR}/_ext/1886890887/wolfmath.o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ${OBJECTDIR}/_ext/1018779441/fips202.o ${OBJECTDIR}/_ext/1018779441/hash.o ${OBJECTDIR}/_ext/1018779441/hash_address.o ${OBJECTDIR}/_ext/1018779441/params.o ${OBJECTDIR}/_ext/1018779441/randombytes.o ${OBJECTDIR}/_ext/1018779441/utils.o ${OBJECTDIR}/_ext/1018779441/wots.o ${OBJECTDIR}/_ext/1018779441/xmss.o ${OBJECTDIR}/_ext/1018779441/xmss_commons.o ${OBJECTDIR}/_ext/1018779441/xmss_core.o ${OBJECTDIR}/_ext/1018779441/xmss_core_fast.o ${OBJECTDIR}/_ext/1018779441/xmss_rand_openssl_aes.o ${OBJECTDIR}/_ext/1789531602/wots_test.o ${OBJECTDIR}/_ext/1789531602/xmss_determinism_test.o ${OBJECTDIR}/_ext/1789531602/xmss_test.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1886890887/misc.o.d ${OBJECTDIR}/_ext/1108542646/bsp.o.d ${OBJECTDIR}/_ext/1632208797/sys_clk_pic32mz.o.d ${OBJECTDIR}/_ext/2139738052/sys_devcon.o.d ${OBJECTDIR}/_ext/2139738052/sys_devcon_pic32mz.o.d ${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o.d ${OBJECTDIR}/_ext/602991347/sys_ports_static.o.d ${OBJECTDIR}/_ext/625345382/system_init.o.d ${OBJECTDIR}/_ext/625345382/system_interrupt.o.d ${OBJECTDIR}/_ext/625345382/system_exceptions.o.d ${OBJECTDIR}/_ext/625345382/system_tasks.o.d ${OBJECTDIR}/_ext/1360937237/app.o.d ${OBJECTDIR}/_ext/1360937237/main.o.d ${OBJECTDIR}/_ext/1606841020/adler32.o.d ${OBJECTDIR}/_ext/1606841020/compress.o.d ${OBJECTDIR}/_ext/1606841020/crc32.o.d ${OBJECTDIR}/_ext/1606841020/deflate.o.d ${OBJECTDIR}/_ext/1606841020/infback.o.d ${OBJECTDIR}/_ext/1606841020/inffast.o.d ${OBJECTDIR}/_ext/1606841020/inflate.o.d ${OBJECTDIR}/_ext/1606841020/inftrees.o.d ${OBJECTDIR}/_ext/1606841020/trees.o.d ${OBJECTDIR}/_ext/1606841020/uncompr.o.d ${OBJECTDIR}/_ext/1606841020/zutil.o.d ${OBJECTDIR}/_ext/1886890887/ecc.o.d ${OBJECTDIR}/_ext/1886890887/arc4.o.d ${OBJECTDIR}/_ext/1886890887/pwdbased.o.d ${OBJECTDIR}/_ext/1886890887/tfm.o.d ${OBJECTDIR}/_ext/1886890887/asn.o.d ${OBJECTDIR}/_ext/1886890887/des3.o.d ${OBJECTDIR}/_ext/1886890887/rsa.o.d ${OBJECTDIR}/_ext/1886890887/aes.o.d ${OBJECTDIR}/_ext/1886890887/md5.o.d ${OBJECTDIR}/_ext/1886890887/sha.o.d ${OBJECTDIR}/_ext/1886890887/sha256.o.d ${OBJECTDIR}/_ext/1886890887/sha512.o.d ${OBJECTDIR}/_ext/1886890887/hmac.o.d ${OBJECTDIR}/_ext/1886890887/hash.o.d ${OBJECTDIR}/_ext/1886890887/compress.o.d ${OBJECTDIR}/_ext/1886890887/random.o.d ${OBJECTDIR}/_ext/1886890887/crypto.o.d ${OBJECTDIR}/_ext/1886890887/coding.o.d ${OBJECTDIR}/_ext/1886890887/error.o.d ${OBJECTDIR}/_ext/1886890887/integer.o.d ${OBJECTDIR}/_ext/1886890887/logging.o.d ${OBJECTDIR}/_ext/1886890887/memory.o.d ${OBJECTDIR}/_ext/1886890887/wc_port.o.d ${OBJECTDIR}/_ext/1886890887/wolfmath.o.d ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d ${OBJECTDIR}/_ext/1018779441/fips202.o.d ${OBJECTDIR}/_ext/1018779441/hash.o.d ${OBJECTDIR}/_ext/1018779441/hash_address.o.d ${OBJECTDIR}/_ext/1018779441/params.o.d ${OBJECTDIR}/_ext/1018779441/randombytes.o.d ${OBJECTDIR}/_ext/1018779441/utils.o.d ${OBJECTDIR}/_ext/1018779441/wots.o.d ${OBJECTDIR}/_ext/1018779441/xmss.o.d ${OBJECTDIR}/_ext/1018779441/xmss_commons.o.d ${OBJECTDIR}/_ext/1018779441/xmss_core.o.d ${OBJECTDIR}/_ext/1018779441/xmss_core_fast.o.d ${OBJECTDIR}/_ext/1018779441/xmss_rand_openssl_aes.o.d ${OBJECTDIR}/_ext/1789531602/wots_test.o.d ${OBJECTDIR}/_ext/1789531602/xmss_determinism_test.o.d ${OBJECTDIR}/_ext/1789531602/xmss_test.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1886890887/misc.o ${OBJECTDIR}/_ext/1108542646/bsp.o ${OBJECTDIR}/_ext/1632208797/sys_clk_pic32mz.o ${OBJECTDIR}/_ext/2139738052/sys_devcon.o ${OBJECTDIR}/_ext/2139738052/sys_devcon_pic32mz.o ${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o ${OBJECTDIR}/_ext/602991347/sys_ports_static.o ${OBJECTDIR}/_ext/625345382/system_init.o ${OBJECTDIR}/_ext/625345382/system_interrupt.o ${OBJECTDIR}/_ext/625345382/system_exceptions.o ${OBJECTDIR}/_ext/625345382/system_tasks.o ${OBJECTDIR}/_ext/1360937237/app.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1606841020/adler32.o ${OBJECTDIR}/_ext/1606841020/compress.o ${OBJECTDIR}/_ext/1606841020/crc32.o ${OBJECTDIR}/_ext/1606841020/deflate.o ${OBJECTDIR}/_ext/1606841020/infback.o ${OBJECTDIR}/_ext/1606841020/inffast.o ${OBJECTDIR}/_ext/1606841020/inflate.o ${OBJECTDIR}/_ext/1606841020/inftrees.o ${OBJECTDIR}/_ext/1606841020/trees.o ${OBJECTDIR}/_ext/1606841020/uncompr.o ${OBJECTDIR}/_ext/1606841020/zutil.o ${OBJECTDIR}/_ext/1886890887/ecc.o ${OBJECTDIR}/_ext/1886890887/arc4.o ${OBJECTDIR}/_ext/1886890887/pwdbased.o ${OBJECTDIR}/_ext/1886890887/tfm.o ${OBJECTDIR}/_ext/1886890887/asn.o ${OBJECTDIR}/_ext/1886890887/des3.o ${OBJECTDIR}/_ext/1886890887/rsa.o ${OBJECTDIR}/_ext/1886890887/aes.o ${OBJECTDIR}/_ext/1886890887/md5.o ${OBJECTDIR}/_ext/1886890887/sha.o ${OBJECTDIR}/_ext/1886890887/sha256.o ${OBJECTDIR}/_ext/1886890887/sha512.o ${OBJECTDIR}/_ext/1886890887/hmac.o ${OBJECTDIR}/_ext/1886890887/hash.o ${OBJECTDIR}/_ext/1886890887/compress.o ${OBJECTDIR}/_ext/1886890887/random.o ${OBJECTDIR}/_ext/1886890887/crypto.o ${OBJECTDIR}/_ext/1886890887/coding.o ${OBJECTDIR}/_ext/1886890887/error.o ${OBJECTDIR}/_ext/1886890887/integer.o ${OBJECTDIR}/_ext/1886890887/logging.o ${OBJECTDIR}/_ext/1886890887/memory.o ${OBJECTDIR}/_ext/1886890887/wc_port.o ${OBJECTDIR}/_ext/1886890887/wolfmath.o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ${OBJECTDIR}/_ext/1018779441/fips202.o ${OBJECTDIR}/_ext/1018779441/hash.o ${OBJECTDIR}/_ext/1018779441/hash_address.o ${OBJECTDIR}/_ext/1018779441/params.o ${OBJECTDIR}/_ext/1018779441/randombytes.o ${OBJECTDIR}/_ext/1018779441/utils.o ${OBJECTDIR}/_ext/1018779441/wots.o ${OBJECTDIR}/_ext/1018779441/xmss.o ${OBJECTDIR}/_ext/1018779441/xmss_commons.o ${OBJECTDIR}/_ext/1018779441/xmss_core.o ${OBJECTDIR}/_ext/1018779441/xmss_core_fast.o ${OBJECTDIR}/_ext/1018779441/xmss_rand_openssl_aes.o ${OBJECTDIR}/_ext/1789531602/wots_test.o ${OBJECTDIR}/_ext/1789531602/xmss_determinism_test.o ${OBJECTDIR}/_ext/1789531602/xmss_test.o

# Source Files
SOURCEFILES=../../../../framework/crypto/src/misc.c ../src/system_config/pic32_mz/bsp/bsp.c ../src/system_config/pic32_mz/framework/system/clk/src/sys_clk_pic32mz.c ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon.c ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon_pic32mz.c ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon_cache_pic32mz.S ../src/system_config/pic32_mz/framework/system/ports/src/sys_ports_static.c ../src/system_config/pic32_mz/system_init.c ../src/system_config/pic32_mz/system_interrupt.c ../src/system_config/pic32_mz/system_exceptions.c ../src/system_config/pic32_mz/system_tasks.c ../src/app.c ../src/main.c ../../../../framework/crypto/src/zlib-1.2.7/adler32.c ../../../../framework/crypto/src/zlib-1.2.7/compress.c ../../../../framework/crypto/src/zlib-1.2.7/crc32.c ../../../../framework/crypto/src/zlib-1.2.7/deflate.c ../../../../framework/crypto/src/zlib-1.2.7/infback.c ../../../../framework/crypto/src/zlib-1.2.7/inffast.c ../../../../framework/crypto/src/zlib-1.2.7/inflate.c ../../../../framework/crypto/src/zlib-1.2.7/inftrees.c ../../../../framework/crypto/src/zlib-1.2.7/trees.c ../../../../framework/crypto/src/zlib-1.2.7/uncompr.c ../../../../framework/crypto/src/zlib-1.2.7/zutil.c ../../../../framework/crypto/src/ecc.c ../../../../framework/crypto/src/arc4.c ../../../../framework/crypto/src/pwdbased.c ../../../../framework/crypto/src/tfm.c ../../../../framework/crypto/src/asn.c ../../../../framework/crypto/src/des3.c ../../../../framework/crypto/src/rsa.c ../../../../framework/crypto/src/aes.c ../../../../framework/crypto/src/md5.c ../../../../framework/crypto/src/sha.c ../../../../framework/crypto/src/sha256.c ../../../../framework/crypto/src/sha512.c ../../../../framework/crypto/src/hmac.c ../../../../framework/crypto/src/hash.c ../../../../framework/crypto/src/compress.c ../../../../framework/crypto/src/random.c ../../../../framework/crypto/src/crypto.c ../../../../framework/crypto/src/coding.c ../../../../framework/crypto/src/error.c ../../../../framework/crypto/src/integer.c ../../../../framework/crypto/src/logging.c ../../../../framework/crypto/src/memory.c ../../../../framework/crypto/src/wc_port.c ../../../../framework/crypto/src/wolfmath.c ../../../../framework/system/int/src/sys_int_pic32.c ../src/xmss/fips202.c ../src/xmss/hash.c ../src/xmss/hash_address.c ../src/xmss/params.c ../src/xmss/randombytes.c ../src/xmss/utils.c ../src/xmss/wots.c ../src/xmss/xmss.c ../src/xmss/xmss_commons.c ../src/xmss/xmss_core.c ../src/xmss/xmss_core_fast.c ../src/xmss/xmss_rand_openssl_aes.c ../src/xmss/test/wots_test.c ../src/xmss/test/xmss_determinism_test.c ../src/xmss/test/xmss_test.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-pic32_mz.mk dist/${CND_CONF}/${IMAGE_TYPE}/pic32_xmss.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MZ2064DAH169
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o: ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2139738052" 
	@${RM} ${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o 
	@${RM} ${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o.ok ${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o.d" "${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1 -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o.d"  -o ${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o.asm.d",--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_SIMULATOR=1
	
else
${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o: ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2139738052" 
	@${RM} ${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o 
	@${RM} ${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o.ok ${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o.d" "${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o.d"  -o ${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/2139738052/sys_devcon_cache_pic32mz.o.asm.d",--gdwarf-2
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1886890887/misc.o: ../../../../framework/crypto/src/misc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/misc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/misc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/misc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/misc.o.d" -o ${OBJECTDIR}/_ext/1886890887/misc.o ../../../../framework/crypto/src/misc.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1108542646/bsp.o: ../src/system_config/pic32_mz/bsp/bsp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1108542646" 
	@${RM} ${OBJECTDIR}/_ext/1108542646/bsp.o.d 
	@${RM} ${OBJECTDIR}/_ext/1108542646/bsp.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1108542646/bsp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1108542646/bsp.o.d" -o ${OBJECTDIR}/_ext/1108542646/bsp.o ../src/system_config/pic32_mz/bsp/bsp.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1632208797/sys_clk_pic32mz.o: ../src/system_config/pic32_mz/framework/system/clk/src/sys_clk_pic32mz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1632208797" 
	@${RM} ${OBJECTDIR}/_ext/1632208797/sys_clk_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/1632208797/sys_clk_pic32mz.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1632208797/sys_clk_pic32mz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1632208797/sys_clk_pic32mz.o.d" -o ${OBJECTDIR}/_ext/1632208797/sys_clk_pic32mz.o ../src/system_config/pic32_mz/framework/system/clk/src/sys_clk_pic32mz.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/2139738052/sys_devcon.o: ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2139738052" 
	@${RM} ${OBJECTDIR}/_ext/2139738052/sys_devcon.o.d 
	@${RM} ${OBJECTDIR}/_ext/2139738052/sys_devcon.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2139738052/sys_devcon.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/2139738052/sys_devcon.o.d" -o ${OBJECTDIR}/_ext/2139738052/sys_devcon.o ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/2139738052/sys_devcon_pic32mz.o: ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon_pic32mz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2139738052" 
	@${RM} ${OBJECTDIR}/_ext/2139738052/sys_devcon_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/2139738052/sys_devcon_pic32mz.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2139738052/sys_devcon_pic32mz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/2139738052/sys_devcon_pic32mz.o.d" -o ${OBJECTDIR}/_ext/2139738052/sys_devcon_pic32mz.o ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon_pic32mz.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/602991347/sys_ports_static.o: ../src/system_config/pic32_mz/framework/system/ports/src/sys_ports_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/602991347" 
	@${RM} ${OBJECTDIR}/_ext/602991347/sys_ports_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/602991347/sys_ports_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/602991347/sys_ports_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/602991347/sys_ports_static.o.d" -o ${OBJECTDIR}/_ext/602991347/sys_ports_static.o ../src/system_config/pic32_mz/framework/system/ports/src/sys_ports_static.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/625345382/system_init.o: ../src/system_config/pic32_mz/system_init.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/625345382" 
	@${RM} ${OBJECTDIR}/_ext/625345382/system_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/625345382/system_init.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/625345382/system_init.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/625345382/system_init.o.d" -o ${OBJECTDIR}/_ext/625345382/system_init.o ../src/system_config/pic32_mz/system_init.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/625345382/system_interrupt.o: ../src/system_config/pic32_mz/system_interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/625345382" 
	@${RM} ${OBJECTDIR}/_ext/625345382/system_interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/625345382/system_interrupt.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/625345382/system_interrupt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/625345382/system_interrupt.o.d" -o ${OBJECTDIR}/_ext/625345382/system_interrupt.o ../src/system_config/pic32_mz/system_interrupt.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/625345382/system_exceptions.o: ../src/system_config/pic32_mz/system_exceptions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/625345382" 
	@${RM} ${OBJECTDIR}/_ext/625345382/system_exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/625345382/system_exceptions.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/625345382/system_exceptions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/625345382/system_exceptions.o.d" -o ${OBJECTDIR}/_ext/625345382/system_exceptions.o ../src/system_config/pic32_mz/system_exceptions.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/625345382/system_tasks.o: ../src/system_config/pic32_mz/system_tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/625345382" 
	@${RM} ${OBJECTDIR}/_ext/625345382/system_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/625345382/system_tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/625345382/system_tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/625345382/system_tasks.o.d" -o ${OBJECTDIR}/_ext/625345382/system_tasks.o ../src/system_config/pic32_mz/system_tasks.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/app.o: ../src/app.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/app.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1360937237/app.o.d" -o ${OBJECTDIR}/_ext/1360937237/app.o ../src/app.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/adler32.o: ../../../../framework/crypto/src/zlib-1.2.7/adler32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/adler32.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/adler32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/adler32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/adler32.o.d" -o ${OBJECTDIR}/_ext/1606841020/adler32.o ../../../../framework/crypto/src/zlib-1.2.7/adler32.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/compress.o: ../../../../framework/crypto/src/zlib-1.2.7/compress.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/compress.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/compress.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/compress.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/compress.o.d" -o ${OBJECTDIR}/_ext/1606841020/compress.o ../../../../framework/crypto/src/zlib-1.2.7/compress.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/crc32.o: ../../../../framework/crypto/src/zlib-1.2.7/crc32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/crc32.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/crc32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/crc32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/crc32.o.d" -o ${OBJECTDIR}/_ext/1606841020/crc32.o ../../../../framework/crypto/src/zlib-1.2.7/crc32.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/deflate.o: ../../../../framework/crypto/src/zlib-1.2.7/deflate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/deflate.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/deflate.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/deflate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/deflate.o.d" -o ${OBJECTDIR}/_ext/1606841020/deflate.o ../../../../framework/crypto/src/zlib-1.2.7/deflate.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/infback.o: ../../../../framework/crypto/src/zlib-1.2.7/infback.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/infback.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/infback.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/infback.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/infback.o.d" -o ${OBJECTDIR}/_ext/1606841020/infback.o ../../../../framework/crypto/src/zlib-1.2.7/infback.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/inffast.o: ../../../../framework/crypto/src/zlib-1.2.7/inffast.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/inffast.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/inffast.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/inffast.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/inffast.o.d" -o ${OBJECTDIR}/_ext/1606841020/inffast.o ../../../../framework/crypto/src/zlib-1.2.7/inffast.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/inflate.o: ../../../../framework/crypto/src/zlib-1.2.7/inflate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/inflate.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/inflate.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/inflate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/inflate.o.d" -o ${OBJECTDIR}/_ext/1606841020/inflate.o ../../../../framework/crypto/src/zlib-1.2.7/inflate.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/inftrees.o: ../../../../framework/crypto/src/zlib-1.2.7/inftrees.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/inftrees.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/inftrees.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/inftrees.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/inftrees.o.d" -o ${OBJECTDIR}/_ext/1606841020/inftrees.o ../../../../framework/crypto/src/zlib-1.2.7/inftrees.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/trees.o: ../../../../framework/crypto/src/zlib-1.2.7/trees.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/trees.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/trees.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/trees.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/trees.o.d" -o ${OBJECTDIR}/_ext/1606841020/trees.o ../../../../framework/crypto/src/zlib-1.2.7/trees.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/uncompr.o: ../../../../framework/crypto/src/zlib-1.2.7/uncompr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/uncompr.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/uncompr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/uncompr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/uncompr.o.d" -o ${OBJECTDIR}/_ext/1606841020/uncompr.o ../../../../framework/crypto/src/zlib-1.2.7/uncompr.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/zutil.o: ../../../../framework/crypto/src/zlib-1.2.7/zutil.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/zutil.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/zutil.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/zutil.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/zutil.o.d" -o ${OBJECTDIR}/_ext/1606841020/zutil.o ../../../../framework/crypto/src/zlib-1.2.7/zutil.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/ecc.o: ../../../../framework/crypto/src/ecc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/ecc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/ecc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/ecc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/ecc.o.d" -o ${OBJECTDIR}/_ext/1886890887/ecc.o ../../../../framework/crypto/src/ecc.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/arc4.o: ../../../../framework/crypto/src/arc4.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/arc4.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/arc4.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/arc4.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/arc4.o.d" -o ${OBJECTDIR}/_ext/1886890887/arc4.o ../../../../framework/crypto/src/arc4.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/pwdbased.o: ../../../../framework/crypto/src/pwdbased.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/pwdbased.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/pwdbased.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/pwdbased.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/pwdbased.o.d" -o ${OBJECTDIR}/_ext/1886890887/pwdbased.o ../../../../framework/crypto/src/pwdbased.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/tfm.o: ../../../../framework/crypto/src/tfm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/tfm.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/tfm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/tfm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/tfm.o.d" -o ${OBJECTDIR}/_ext/1886890887/tfm.o ../../../../framework/crypto/src/tfm.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/asn.o: ../../../../framework/crypto/src/asn.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/asn.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/asn.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/asn.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/asn.o.d" -o ${OBJECTDIR}/_ext/1886890887/asn.o ../../../../framework/crypto/src/asn.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/des3.o: ../../../../framework/crypto/src/des3.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/des3.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/des3.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/des3.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/des3.o.d" -o ${OBJECTDIR}/_ext/1886890887/des3.o ../../../../framework/crypto/src/des3.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/rsa.o: ../../../../framework/crypto/src/rsa.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/rsa.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/rsa.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/rsa.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/rsa.o.d" -o ${OBJECTDIR}/_ext/1886890887/rsa.o ../../../../framework/crypto/src/rsa.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/aes.o: ../../../../framework/crypto/src/aes.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/aes.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/aes.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/aes.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/aes.o.d" -o ${OBJECTDIR}/_ext/1886890887/aes.o ../../../../framework/crypto/src/aes.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/md5.o: ../../../../framework/crypto/src/md5.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/md5.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/md5.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/md5.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/md5.o.d" -o ${OBJECTDIR}/_ext/1886890887/md5.o ../../../../framework/crypto/src/md5.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/sha.o: ../../../../framework/crypto/src/sha.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/sha.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/sha.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/sha.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/sha.o.d" -o ${OBJECTDIR}/_ext/1886890887/sha.o ../../../../framework/crypto/src/sha.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/sha256.o: ../../../../framework/crypto/src/sha256.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/sha256.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/sha256.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/sha256.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/sha256.o.d" -o ${OBJECTDIR}/_ext/1886890887/sha256.o ../../../../framework/crypto/src/sha256.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/sha512.o: ../../../../framework/crypto/src/sha512.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/sha512.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/sha512.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/sha512.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/sha512.o.d" -o ${OBJECTDIR}/_ext/1886890887/sha512.o ../../../../framework/crypto/src/sha512.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/hmac.o: ../../../../framework/crypto/src/hmac.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/hmac.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/hmac.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/hmac.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/hmac.o.d" -o ${OBJECTDIR}/_ext/1886890887/hmac.o ../../../../framework/crypto/src/hmac.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/hash.o: ../../../../framework/crypto/src/hash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/hash.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/hash.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/hash.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/hash.o.d" -o ${OBJECTDIR}/_ext/1886890887/hash.o ../../../../framework/crypto/src/hash.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/compress.o: ../../../../framework/crypto/src/compress.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/compress.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/compress.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/compress.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/compress.o.d" -o ${OBJECTDIR}/_ext/1886890887/compress.o ../../../../framework/crypto/src/compress.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/random.o: ../../../../framework/crypto/src/random.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/random.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/random.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/random.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/random.o.d" -o ${OBJECTDIR}/_ext/1886890887/random.o ../../../../framework/crypto/src/random.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/crypto.o: ../../../../framework/crypto/src/crypto.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/crypto.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/crypto.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/crypto.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/crypto.o.d" -o ${OBJECTDIR}/_ext/1886890887/crypto.o ../../../../framework/crypto/src/crypto.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/coding.o: ../../../../framework/crypto/src/coding.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/coding.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/coding.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/coding.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/coding.o.d" -o ${OBJECTDIR}/_ext/1886890887/coding.o ../../../../framework/crypto/src/coding.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/error.o: ../../../../framework/crypto/src/error.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/error.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/error.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/error.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/error.o.d" -o ${OBJECTDIR}/_ext/1886890887/error.o ../../../../framework/crypto/src/error.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/integer.o: ../../../../framework/crypto/src/integer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/integer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/integer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/integer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/integer.o.d" -o ${OBJECTDIR}/_ext/1886890887/integer.o ../../../../framework/crypto/src/integer.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/logging.o: ../../../../framework/crypto/src/logging.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/logging.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/logging.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/logging.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/logging.o.d" -o ${OBJECTDIR}/_ext/1886890887/logging.o ../../../../framework/crypto/src/logging.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/memory.o: ../../../../framework/crypto/src/memory.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/memory.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/memory.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/memory.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/memory.o.d" -o ${OBJECTDIR}/_ext/1886890887/memory.o ../../../../framework/crypto/src/memory.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/wc_port.o: ../../../../framework/crypto/src/wc_port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/wc_port.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/wc_port.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/wc_port.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/wc_port.o.d" -o ${OBJECTDIR}/_ext/1886890887/wc_port.o ../../../../framework/crypto/src/wc_port.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/wolfmath.o: ../../../../framework/crypto/src/wolfmath.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/wolfmath.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/wolfmath.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/wolfmath.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/wolfmath.o.d" -o ${OBJECTDIR}/_ext/1886890887/wolfmath.o ../../../../framework/crypto/src/wolfmath.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/122796885/sys_int_pic32.o: ../../../../framework/system/int/src/sys_int_pic32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/122796885" 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" -o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ../../../../framework/system/int/src/sys_int_pic32.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/fips202.o: ../src/xmss/fips202.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/fips202.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/fips202.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/fips202.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/fips202.o.d" -o ${OBJECTDIR}/_ext/1018779441/fips202.o ../src/xmss/fips202.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/hash.o: ../src/xmss/hash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/hash.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/hash.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/hash.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/hash.o.d" -o ${OBJECTDIR}/_ext/1018779441/hash.o ../src/xmss/hash.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/hash_address.o: ../src/xmss/hash_address.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/hash_address.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/hash_address.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/hash_address.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/hash_address.o.d" -o ${OBJECTDIR}/_ext/1018779441/hash_address.o ../src/xmss/hash_address.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/params.o: ../src/xmss/params.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/params.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/params.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/params.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/params.o.d" -o ${OBJECTDIR}/_ext/1018779441/params.o ../src/xmss/params.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/randombytes.o: ../src/xmss/randombytes.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/randombytes.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/randombytes.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/randombytes.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/randombytes.o.d" -o ${OBJECTDIR}/_ext/1018779441/randombytes.o ../src/xmss/randombytes.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/utils.o: ../src/xmss/utils.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/utils.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/utils.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/utils.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/utils.o.d" -o ${OBJECTDIR}/_ext/1018779441/utils.o ../src/xmss/utils.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/wots.o: ../src/xmss/wots.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/wots.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/wots.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/wots.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/wots.o.d" -o ${OBJECTDIR}/_ext/1018779441/wots.o ../src/xmss/wots.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/xmss.o: ../src/xmss/xmss.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/xmss.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/xmss.o.d" -o ${OBJECTDIR}/_ext/1018779441/xmss.o ../src/xmss/xmss.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/xmss_commons.o: ../src/xmss/xmss_commons.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss_commons.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss_commons.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/xmss_commons.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/xmss_commons.o.d" -o ${OBJECTDIR}/_ext/1018779441/xmss_commons.o ../src/xmss/xmss_commons.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/xmss_core.o: ../src/xmss/xmss_core.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss_core.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss_core.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/xmss_core.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/xmss_core.o.d" -o ${OBJECTDIR}/_ext/1018779441/xmss_core.o ../src/xmss/xmss_core.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/xmss_core_fast.o: ../src/xmss/xmss_core_fast.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss_core_fast.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss_core_fast.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/xmss_core_fast.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/xmss_core_fast.o.d" -o ${OBJECTDIR}/_ext/1018779441/xmss_core_fast.o ../src/xmss/xmss_core_fast.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/xmss_rand_openssl_aes.o: ../src/xmss/xmss_rand_openssl_aes.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss_rand_openssl_aes.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss_rand_openssl_aes.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/xmss_rand_openssl_aes.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/xmss_rand_openssl_aes.o.d" -o ${OBJECTDIR}/_ext/1018779441/xmss_rand_openssl_aes.o ../src/xmss/xmss_rand_openssl_aes.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1789531602/wots_test.o: ../src/xmss/test/wots_test.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1789531602" 
	@${RM} ${OBJECTDIR}/_ext/1789531602/wots_test.o.d 
	@${RM} ${OBJECTDIR}/_ext/1789531602/wots_test.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1789531602/wots_test.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1789531602/wots_test.o.d" -o ${OBJECTDIR}/_ext/1789531602/wots_test.o ../src/xmss/test/wots_test.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1789531602/xmss_determinism_test.o: ../src/xmss/test/xmss_determinism_test.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1789531602" 
	@${RM} ${OBJECTDIR}/_ext/1789531602/xmss_determinism_test.o.d 
	@${RM} ${OBJECTDIR}/_ext/1789531602/xmss_determinism_test.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1789531602/xmss_determinism_test.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1789531602/xmss_determinism_test.o.d" -o ${OBJECTDIR}/_ext/1789531602/xmss_determinism_test.o ../src/xmss/test/xmss_determinism_test.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1789531602/xmss_test.o: ../src/xmss/test/xmss_test.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1789531602" 
	@${RM} ${OBJECTDIR}/_ext/1789531602/xmss_test.o.d 
	@${RM} ${OBJECTDIR}/_ext/1789531602/xmss_test.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1789531602/xmss_test.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_SIMULATOR=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1789531602/xmss_test.o.d" -o ${OBJECTDIR}/_ext/1789531602/xmss_test.o ../src/xmss/test/xmss_test.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
else
${OBJECTDIR}/_ext/1886890887/misc.o: ../../../../framework/crypto/src/misc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/misc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/misc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/misc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/misc.o.d" -o ${OBJECTDIR}/_ext/1886890887/misc.o ../../../../framework/crypto/src/misc.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1108542646/bsp.o: ../src/system_config/pic32_mz/bsp/bsp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1108542646" 
	@${RM} ${OBJECTDIR}/_ext/1108542646/bsp.o.d 
	@${RM} ${OBJECTDIR}/_ext/1108542646/bsp.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1108542646/bsp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1108542646/bsp.o.d" -o ${OBJECTDIR}/_ext/1108542646/bsp.o ../src/system_config/pic32_mz/bsp/bsp.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1632208797/sys_clk_pic32mz.o: ../src/system_config/pic32_mz/framework/system/clk/src/sys_clk_pic32mz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1632208797" 
	@${RM} ${OBJECTDIR}/_ext/1632208797/sys_clk_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/1632208797/sys_clk_pic32mz.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1632208797/sys_clk_pic32mz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1632208797/sys_clk_pic32mz.o.d" -o ${OBJECTDIR}/_ext/1632208797/sys_clk_pic32mz.o ../src/system_config/pic32_mz/framework/system/clk/src/sys_clk_pic32mz.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/2139738052/sys_devcon.o: ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2139738052" 
	@${RM} ${OBJECTDIR}/_ext/2139738052/sys_devcon.o.d 
	@${RM} ${OBJECTDIR}/_ext/2139738052/sys_devcon.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2139738052/sys_devcon.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/2139738052/sys_devcon.o.d" -o ${OBJECTDIR}/_ext/2139738052/sys_devcon.o ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/2139738052/sys_devcon_pic32mz.o: ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon_pic32mz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/2139738052" 
	@${RM} ${OBJECTDIR}/_ext/2139738052/sys_devcon_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/2139738052/sys_devcon_pic32mz.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2139738052/sys_devcon_pic32mz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/2139738052/sys_devcon_pic32mz.o.d" -o ${OBJECTDIR}/_ext/2139738052/sys_devcon_pic32mz.o ../src/system_config/pic32_mz/framework/system/devcon/src/sys_devcon_pic32mz.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/602991347/sys_ports_static.o: ../src/system_config/pic32_mz/framework/system/ports/src/sys_ports_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/602991347" 
	@${RM} ${OBJECTDIR}/_ext/602991347/sys_ports_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/602991347/sys_ports_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/602991347/sys_ports_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/602991347/sys_ports_static.o.d" -o ${OBJECTDIR}/_ext/602991347/sys_ports_static.o ../src/system_config/pic32_mz/framework/system/ports/src/sys_ports_static.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/625345382/system_init.o: ../src/system_config/pic32_mz/system_init.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/625345382" 
	@${RM} ${OBJECTDIR}/_ext/625345382/system_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/625345382/system_init.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/625345382/system_init.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/625345382/system_init.o.d" -o ${OBJECTDIR}/_ext/625345382/system_init.o ../src/system_config/pic32_mz/system_init.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/625345382/system_interrupt.o: ../src/system_config/pic32_mz/system_interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/625345382" 
	@${RM} ${OBJECTDIR}/_ext/625345382/system_interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/625345382/system_interrupt.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/625345382/system_interrupt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/625345382/system_interrupt.o.d" -o ${OBJECTDIR}/_ext/625345382/system_interrupt.o ../src/system_config/pic32_mz/system_interrupt.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/625345382/system_exceptions.o: ../src/system_config/pic32_mz/system_exceptions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/625345382" 
	@${RM} ${OBJECTDIR}/_ext/625345382/system_exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/625345382/system_exceptions.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/625345382/system_exceptions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/625345382/system_exceptions.o.d" -o ${OBJECTDIR}/_ext/625345382/system_exceptions.o ../src/system_config/pic32_mz/system_exceptions.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/625345382/system_tasks.o: ../src/system_config/pic32_mz/system_tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/625345382" 
	@${RM} ${OBJECTDIR}/_ext/625345382/system_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/625345382/system_tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/625345382/system_tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/625345382/system_tasks.o.d" -o ${OBJECTDIR}/_ext/625345382/system_tasks.o ../src/system_config/pic32_mz/system_tasks.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/app.o: ../src/app.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/app.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1360937237/app.o.d" -o ${OBJECTDIR}/_ext/1360937237/app.o ../src/app.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/adler32.o: ../../../../framework/crypto/src/zlib-1.2.7/adler32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/adler32.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/adler32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/adler32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/adler32.o.d" -o ${OBJECTDIR}/_ext/1606841020/adler32.o ../../../../framework/crypto/src/zlib-1.2.7/adler32.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/compress.o: ../../../../framework/crypto/src/zlib-1.2.7/compress.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/compress.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/compress.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/compress.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/compress.o.d" -o ${OBJECTDIR}/_ext/1606841020/compress.o ../../../../framework/crypto/src/zlib-1.2.7/compress.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/crc32.o: ../../../../framework/crypto/src/zlib-1.2.7/crc32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/crc32.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/crc32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/crc32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/crc32.o.d" -o ${OBJECTDIR}/_ext/1606841020/crc32.o ../../../../framework/crypto/src/zlib-1.2.7/crc32.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/deflate.o: ../../../../framework/crypto/src/zlib-1.2.7/deflate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/deflate.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/deflate.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/deflate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/deflate.o.d" -o ${OBJECTDIR}/_ext/1606841020/deflate.o ../../../../framework/crypto/src/zlib-1.2.7/deflate.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/infback.o: ../../../../framework/crypto/src/zlib-1.2.7/infback.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/infback.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/infback.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/infback.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/infback.o.d" -o ${OBJECTDIR}/_ext/1606841020/infback.o ../../../../framework/crypto/src/zlib-1.2.7/infback.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/inffast.o: ../../../../framework/crypto/src/zlib-1.2.7/inffast.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/inffast.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/inffast.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/inffast.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/inffast.o.d" -o ${OBJECTDIR}/_ext/1606841020/inffast.o ../../../../framework/crypto/src/zlib-1.2.7/inffast.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/inflate.o: ../../../../framework/crypto/src/zlib-1.2.7/inflate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/inflate.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/inflate.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/inflate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/inflate.o.d" -o ${OBJECTDIR}/_ext/1606841020/inflate.o ../../../../framework/crypto/src/zlib-1.2.7/inflate.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/inftrees.o: ../../../../framework/crypto/src/zlib-1.2.7/inftrees.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/inftrees.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/inftrees.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/inftrees.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/inftrees.o.d" -o ${OBJECTDIR}/_ext/1606841020/inftrees.o ../../../../framework/crypto/src/zlib-1.2.7/inftrees.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/trees.o: ../../../../framework/crypto/src/zlib-1.2.7/trees.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/trees.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/trees.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/trees.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/trees.o.d" -o ${OBJECTDIR}/_ext/1606841020/trees.o ../../../../framework/crypto/src/zlib-1.2.7/trees.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/uncompr.o: ../../../../framework/crypto/src/zlib-1.2.7/uncompr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/uncompr.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/uncompr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/uncompr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/uncompr.o.d" -o ${OBJECTDIR}/_ext/1606841020/uncompr.o ../../../../framework/crypto/src/zlib-1.2.7/uncompr.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1606841020/zutil.o: ../../../../framework/crypto/src/zlib-1.2.7/zutil.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1606841020" 
	@${RM} ${OBJECTDIR}/_ext/1606841020/zutil.o.d 
	@${RM} ${OBJECTDIR}/_ext/1606841020/zutil.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1606841020/zutil.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1606841020/zutil.o.d" -o ${OBJECTDIR}/_ext/1606841020/zutil.o ../../../../framework/crypto/src/zlib-1.2.7/zutil.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/ecc.o: ../../../../framework/crypto/src/ecc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/ecc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/ecc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/ecc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/ecc.o.d" -o ${OBJECTDIR}/_ext/1886890887/ecc.o ../../../../framework/crypto/src/ecc.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/arc4.o: ../../../../framework/crypto/src/arc4.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/arc4.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/arc4.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/arc4.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/arc4.o.d" -o ${OBJECTDIR}/_ext/1886890887/arc4.o ../../../../framework/crypto/src/arc4.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/pwdbased.o: ../../../../framework/crypto/src/pwdbased.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/pwdbased.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/pwdbased.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/pwdbased.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/pwdbased.o.d" -o ${OBJECTDIR}/_ext/1886890887/pwdbased.o ../../../../framework/crypto/src/pwdbased.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/tfm.o: ../../../../framework/crypto/src/tfm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/tfm.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/tfm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/tfm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/tfm.o.d" -o ${OBJECTDIR}/_ext/1886890887/tfm.o ../../../../framework/crypto/src/tfm.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/asn.o: ../../../../framework/crypto/src/asn.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/asn.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/asn.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/asn.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/asn.o.d" -o ${OBJECTDIR}/_ext/1886890887/asn.o ../../../../framework/crypto/src/asn.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/des3.o: ../../../../framework/crypto/src/des3.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/des3.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/des3.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/des3.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/des3.o.d" -o ${OBJECTDIR}/_ext/1886890887/des3.o ../../../../framework/crypto/src/des3.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/rsa.o: ../../../../framework/crypto/src/rsa.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/rsa.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/rsa.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/rsa.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/rsa.o.d" -o ${OBJECTDIR}/_ext/1886890887/rsa.o ../../../../framework/crypto/src/rsa.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/aes.o: ../../../../framework/crypto/src/aes.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/aes.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/aes.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/aes.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/aes.o.d" -o ${OBJECTDIR}/_ext/1886890887/aes.o ../../../../framework/crypto/src/aes.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/md5.o: ../../../../framework/crypto/src/md5.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/md5.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/md5.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/md5.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/md5.o.d" -o ${OBJECTDIR}/_ext/1886890887/md5.o ../../../../framework/crypto/src/md5.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/sha.o: ../../../../framework/crypto/src/sha.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/sha.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/sha.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/sha.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/sha.o.d" -o ${OBJECTDIR}/_ext/1886890887/sha.o ../../../../framework/crypto/src/sha.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/sha256.o: ../../../../framework/crypto/src/sha256.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/sha256.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/sha256.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/sha256.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/sha256.o.d" -o ${OBJECTDIR}/_ext/1886890887/sha256.o ../../../../framework/crypto/src/sha256.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/sha512.o: ../../../../framework/crypto/src/sha512.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/sha512.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/sha512.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/sha512.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/sha512.o.d" -o ${OBJECTDIR}/_ext/1886890887/sha512.o ../../../../framework/crypto/src/sha512.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/hmac.o: ../../../../framework/crypto/src/hmac.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/hmac.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/hmac.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/hmac.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/hmac.o.d" -o ${OBJECTDIR}/_ext/1886890887/hmac.o ../../../../framework/crypto/src/hmac.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/hash.o: ../../../../framework/crypto/src/hash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/hash.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/hash.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/hash.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/hash.o.d" -o ${OBJECTDIR}/_ext/1886890887/hash.o ../../../../framework/crypto/src/hash.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/compress.o: ../../../../framework/crypto/src/compress.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/compress.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/compress.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/compress.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/compress.o.d" -o ${OBJECTDIR}/_ext/1886890887/compress.o ../../../../framework/crypto/src/compress.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/random.o: ../../../../framework/crypto/src/random.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/random.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/random.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/random.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/random.o.d" -o ${OBJECTDIR}/_ext/1886890887/random.o ../../../../framework/crypto/src/random.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/crypto.o: ../../../../framework/crypto/src/crypto.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/crypto.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/crypto.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/crypto.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/crypto.o.d" -o ${OBJECTDIR}/_ext/1886890887/crypto.o ../../../../framework/crypto/src/crypto.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/coding.o: ../../../../framework/crypto/src/coding.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/coding.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/coding.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/coding.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/coding.o.d" -o ${OBJECTDIR}/_ext/1886890887/coding.o ../../../../framework/crypto/src/coding.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/error.o: ../../../../framework/crypto/src/error.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/error.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/error.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/error.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/error.o.d" -o ${OBJECTDIR}/_ext/1886890887/error.o ../../../../framework/crypto/src/error.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/integer.o: ../../../../framework/crypto/src/integer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/integer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/integer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/integer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/integer.o.d" -o ${OBJECTDIR}/_ext/1886890887/integer.o ../../../../framework/crypto/src/integer.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/logging.o: ../../../../framework/crypto/src/logging.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/logging.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/logging.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/logging.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/logging.o.d" -o ${OBJECTDIR}/_ext/1886890887/logging.o ../../../../framework/crypto/src/logging.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/memory.o: ../../../../framework/crypto/src/memory.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/memory.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/memory.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/memory.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/memory.o.d" -o ${OBJECTDIR}/_ext/1886890887/memory.o ../../../../framework/crypto/src/memory.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/wc_port.o: ../../../../framework/crypto/src/wc_port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/wc_port.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/wc_port.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/wc_port.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/wc_port.o.d" -o ${OBJECTDIR}/_ext/1886890887/wc_port.o ../../../../framework/crypto/src/wc_port.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1886890887/wolfmath.o: ../../../../framework/crypto/src/wolfmath.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1886890887" 
	@${RM} ${OBJECTDIR}/_ext/1886890887/wolfmath.o.d 
	@${RM} ${OBJECTDIR}/_ext/1886890887/wolfmath.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1886890887/wolfmath.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1886890887/wolfmath.o.d" -o ${OBJECTDIR}/_ext/1886890887/wolfmath.o ../../../../framework/crypto/src/wolfmath.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/122796885/sys_int_pic32.o: ../../../../framework/system/int/src/sys_int_pic32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/122796885" 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" -o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ../../../../framework/system/int/src/sys_int_pic32.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/fips202.o: ../src/xmss/fips202.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/fips202.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/fips202.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/fips202.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/fips202.o.d" -o ${OBJECTDIR}/_ext/1018779441/fips202.o ../src/xmss/fips202.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/hash.o: ../src/xmss/hash.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/hash.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/hash.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/hash.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/hash.o.d" -o ${OBJECTDIR}/_ext/1018779441/hash.o ../src/xmss/hash.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/hash_address.o: ../src/xmss/hash_address.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/hash_address.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/hash_address.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/hash_address.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/hash_address.o.d" -o ${OBJECTDIR}/_ext/1018779441/hash_address.o ../src/xmss/hash_address.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/params.o: ../src/xmss/params.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/params.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/params.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/params.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/params.o.d" -o ${OBJECTDIR}/_ext/1018779441/params.o ../src/xmss/params.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/randombytes.o: ../src/xmss/randombytes.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/randombytes.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/randombytes.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/randombytes.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/randombytes.o.d" -o ${OBJECTDIR}/_ext/1018779441/randombytes.o ../src/xmss/randombytes.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/utils.o: ../src/xmss/utils.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/utils.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/utils.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/utils.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/utils.o.d" -o ${OBJECTDIR}/_ext/1018779441/utils.o ../src/xmss/utils.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/wots.o: ../src/xmss/wots.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/wots.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/wots.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/wots.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/wots.o.d" -o ${OBJECTDIR}/_ext/1018779441/wots.o ../src/xmss/wots.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/xmss.o: ../src/xmss/xmss.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/xmss.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/xmss.o.d" -o ${OBJECTDIR}/_ext/1018779441/xmss.o ../src/xmss/xmss.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/xmss_commons.o: ../src/xmss/xmss_commons.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss_commons.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss_commons.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/xmss_commons.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/xmss_commons.o.d" -o ${OBJECTDIR}/_ext/1018779441/xmss_commons.o ../src/xmss/xmss_commons.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/xmss_core.o: ../src/xmss/xmss_core.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss_core.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss_core.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/xmss_core.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/xmss_core.o.d" -o ${OBJECTDIR}/_ext/1018779441/xmss_core.o ../src/xmss/xmss_core.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/xmss_core_fast.o: ../src/xmss/xmss_core_fast.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss_core_fast.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss_core_fast.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/xmss_core_fast.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/xmss_core_fast.o.d" -o ${OBJECTDIR}/_ext/1018779441/xmss_core_fast.o ../src/xmss/xmss_core_fast.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1018779441/xmss_rand_openssl_aes.o: ../src/xmss/xmss_rand_openssl_aes.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1018779441" 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss_rand_openssl_aes.o.d 
	@${RM} ${OBJECTDIR}/_ext/1018779441/xmss_rand_openssl_aes.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1018779441/xmss_rand_openssl_aes.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1018779441/xmss_rand_openssl_aes.o.d" -o ${OBJECTDIR}/_ext/1018779441/xmss_rand_openssl_aes.o ../src/xmss/xmss_rand_openssl_aes.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1789531602/wots_test.o: ../src/xmss/test/wots_test.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1789531602" 
	@${RM} ${OBJECTDIR}/_ext/1789531602/wots_test.o.d 
	@${RM} ${OBJECTDIR}/_ext/1789531602/wots_test.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1789531602/wots_test.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1789531602/wots_test.o.d" -o ${OBJECTDIR}/_ext/1789531602/wots_test.o ../src/xmss/test/wots_test.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1789531602/xmss_determinism_test.o: ../src/xmss/test/xmss_determinism_test.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1789531602" 
	@${RM} ${OBJECTDIR}/_ext/1789531602/xmss_determinism_test.o.d 
	@${RM} ${OBJECTDIR}/_ext/1789531602/xmss_determinism_test.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1789531602/xmss_determinism_test.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1789531602/xmss_determinism_test.o.d" -o ${OBJECTDIR}/_ext/1789531602/xmss_determinism_test.o ../src/xmss/test/xmss_determinism_test.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1789531602/xmss_test.o: ../src/xmss/test/xmss_test.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1789531602" 
	@${RM} ${OBJECTDIR}/_ext/1789531602/xmss_test.o.d 
	@${RM} ${OBJECTDIR}/_ext/1789531602/xmss_test.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1789531602/xmss_test.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32_mz" -I"../src/pic32_mz" -I"../../../../framework" -I"../src/system_config/pic32_mz/framework" -I"../src/system_config/pic32_mz/bsp" -MMD -MF "${OBJECTDIR}/_ext/1789531602/xmss_test.o.d" -o ${OBJECTDIR}/_ext/1789531602/xmss_test.o ../src/xmss/test/xmss_test.c    -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/pic32_xmss.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../../../../bin/framework/peripheral/PIC32MZ2064DAH169_peripherals.a  
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mdebugger -D__MPLAB_DEBUGGER_SIMULATOR=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/pic32_xmss.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ../../../../bin/framework/peripheral/PIC32MZ2064DAH169_peripherals.a      -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)      -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=__MPLAB_DEBUGGER_SIMULATOR=1,--defsym=_min_heap_size=0,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/pic32_xmss.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../../../../bin/framework/peripheral/PIC32MZ2064DAH169_peripherals.a 
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/pic32_xmss.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ../../../../bin/framework/peripheral/PIC32MZ2064DAH169_peripherals.a      -DXPRJ_pic32_mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=0,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	${MP_CC_DIR}/xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/pic32_xmss.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/pic32_mz
	${RM} -r dist/pic32_mz

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
