# README #

This repo contains an MPLAB Harmony project for building an implementation of XMSS for the PIC32.


### How do I get set up? ###

1. cd into ~/microchip/harmony/v2\_04/apps (or similar directory)
1. git clone this repo
1. open the project from MPLAB IDE X

### Who do I talk to? ###

* John Ratz (john.ratz@envieta.com)
